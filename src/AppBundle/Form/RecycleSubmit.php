<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Recycle Batteries submission form
 *
 * @package AppBundle\Form
 */
class RecycleSubmit extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\RecycleBattery',
        ));
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', Type\TextType::class)
            ->add('count', Type\IntegerType::class)
            ->add('name', Type\TextType::class, array('required' => false))
            ->add('create', Type\SubmitType::class, array('label' => 'Create'))
        ;
    }
}
