<?php

namespace AppBundle\Controller;

use AppBundle\Entity\RecycleBattery;
use AppBundle\Form\RecycleSubmit;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\HttpFoundation\Session\Session;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        // Instantiate form for the recycle request
        $form = $this->createForm(RecycleSubmit::class);

        // Handle incoming $request
        $form->handleRequest($request);

        // Was a valid form submitted?
        if ($form->isValid()) {
            /** @var RecycleBattery $recycleRequest */
            $recycleRequest = $form->getData();

            // Persist recycle-submit request
            $em = $this->getDoctrine()->getManager();
            $em->persist($recycleRequest);
            $em->flush();

            // Adding success flash-message
            /** @var Session $session */
            $session = $this->get('session');
            $session->getFlashBag()->add('success', 'Batteries submitted successfully :)');

            // Redirect to stats page
            return $this->redirectToRoute('stats');
        }

        // Render default view
        return array(
            'form' => $form->createView(),
        );
    }

    /**
     * @Route("/stats", name="stats")
     * @Template()
     */
    public function statsAction(Request $request)
    {
        /** @var \AppBundle\Entity\RecycleRepository $repo */
        $repo = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:RecycleBattery');

        // Render default view
        return array(
            'stats' => $repo->getStats(),
        );
    }
}
