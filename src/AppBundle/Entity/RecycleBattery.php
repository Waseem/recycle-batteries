<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Batteries submission (POPO) model
 *
 * @package AppBundle\Entity
 * @ORM\Entity
 * @ORM\Table(name="recycle_battery")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\RecycleRepository")
 */
class RecycleBattery
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * What kind of batteries were submitted?
     *
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=100)
     */
    protected $type;

    /**
     * How many batteries were submitted?
     *
     * @var int
     * @Assert\NotBlank()
     * @Assert\GreaterThanOrEqual(0)
     * @ORM\Column(type="integer")
     */
    protected $count;

    /**
     * Who submitted these batteries to us?
     *
     * @var string
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $name;

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return RecycleBattery
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param int $count
     *
     * @return RecycleBattery
     */
    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return RecycleBattery
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
}