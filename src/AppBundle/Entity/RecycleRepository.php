<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\EntityRepository;

/**
 * Houses Recycle-Battery related queries
 */
class RecycleRepository extends EntityRepository
{
    /**
     * Gets statistical data about recycled batteries
     *
     * @return array
     */
    public function getStats()
    {
        $qb = $this->createQueryBuilder('rb');

        $qb->select('rb.type')
            ->addSelect('SUM(rb.count) AS total')
            ->groupBy('rb.type')
            ->orderBy('rb.type', 'ASC');

        return $qb->getQuery()->getResult();
    }
}
