<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        // Verify form and elements
        $this->assertContains('Recycle Your Batteries', $crawler->filter('h1')->text());
        $this->assertEquals(1, $crawler->filter('form')->count(), 'Form not found on page');
        $this->assertEquals(2, $crawler->filter('input[type=text]')->count(), 'Expected form fields (Type or Name) are missing');
        $this->assertEquals(1, $crawler->filter('input[type=number]')->count(), 'Expected form field (Count) is missing');
        $this->assertEquals(1, $crawler->filter('button[type=submit]')->count(), 'Form submit button is missing');
    }

    /**
     * @depends testIndex
     */
    public function testFormSubmit()
    {
        $testDataList = [
            ['recycle_submit[type]' => 'AA', 'recycle_submit[count]' => 4, 'recycle_submit[name]' => 'Waseem'],
            ['recycle_submit[type]' => 'AAA', 'recycle_submit[count]' => 3, 'recycle_submit[name]' => 'Anon'],
            ['recycle_submit[type]' => 'AA', 'recycle_submit[count]' => 1, 'recycle_submit[name]' => 'Colin'],
        ];

        $client = static::createClient();

        $crawler = $client->request('POST', '/');

        // Get submit button by ID
        $submitButton = $crawler->selectButton('recycle_submit_create');

        foreach ($testDataList as $testData) {
            // Submit test data
            $client->submit($submitButton->form($testData, 'POST'));
        }

        // Now, fetch stats
        $crawler = $client->request('GET', '/stats');

        $this->assertEquals(5, $crawler->filter('#AA')->text());
        $this->assertEquals(3, $crawler->filter('#AAA')->text());
    }
}
